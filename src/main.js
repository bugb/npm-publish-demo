const argv = require('minimist')(process.argv.slice(2));
const { help } = require('./help');

function fibo() {
  const argument = argv._;
  if (!argument.length) {
    help();
    return;
  }
  const [number] = argument;
  if (!Number.isInteger(number)) {
    console.log('Error: NaN');
    return;
  }
  if (number < 1) {
    console.log('Error: Only positive number is accepted');
    return;
  }
  if (argument.length !== 1) {
    console.log('Error: this program take 1 argument only');
    return;
  }

  if (number < 4) {
    console.log([0, 1, 1][number - 1]);
    return;
  }
  const fibGenerator = function* fibGenerator() {
    let current = 0; let
      next = 1;
    while (true) {
      [next, current] = [next + current, next];
      yield current;
    }
  };

  const fib = fibGenerator();
  for (let i = 1; i < number; i += 1) {
    if (i === number - 1) console.log(fib.next().value);
    // eslint-disable-next-line
    else fib.next().value;
  }
}

module.exports = { fibo };
