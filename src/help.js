const commandLineUsage = require('command-line-usage');

function help() {
  const sections = [
    {
      header: 'A demo npm publish for topgun class - fibonaci',
      content: `You must provide a number
        Eg: fibo 12`,
    },
    {
      header: 'Options',
      optionList: [
        {
          name: 'help',
          description: 'Print this usage guide.',
        },
      ],
    },
  ];
  const usage = commandLineUsage(sections);
  console.log(usage);
}
module.exports = { help };
